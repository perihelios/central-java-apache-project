# Java Stub Project w/Apache-License & Central Repository Deployment #
This is a "stub project" (a template intended as a basis for other projects) with [Gradle](https://www.gradle.org/) scripts to help you deploy artifacts from your project to the [Central Repository](http://search.maven.org/).

**Note:** Deploying artifacts to the Central Repository with this documentation and these scripts only makes sense if your project is Open Source.

## Steps to Use This Stub Project ##
1. Clone the project using your own project name: `git clone https://bitbucket.org/perihelios/central-java-apache-project.git <project-name>`, where *<project-name>* is the name of your new project.
1. Go into your *<project-name>* directory.
1. Remove the `.git` directory. On *nix systems, use `rm -rf .git`; on Windows systems, use `rmdir /s /q .git`.
1. Initialize your project's metadata (this is specific to the Gradle scripts that are part of this stub project, not something you would have done in the past). On *nix systems, use `./gradlew initMetadata`; on Windows systems, use `.\gradlew.bat initMetadata`. Follow the prompts and help text to choose appropriate values.
1. Initialize your source control. For Git, this would be `git init`.
1. Add/commit your files, as appropriate for your source control. For Git, this would be `git add .`.
1. Add a remote (such as a Bitbucket or GitHub project URL), and push your changes.

**Note:** To deploy to the Central Repository, you need to have some project already set up in some public SCM, like Bitbucket or GitHub. Part of the setup process with Sonatype is to provide an Open Source project's SCM URL.

## Steps to Deploy to the Central Repository ##
The [full guide from Sonatype](http://central.sonatype.org/pages/ossrh-guide.html) provides more detail, but the following steps are what you must do. The friendly people at Sonatype have to do some manual steps to set up your deployment access and that can [take a few days](http://central.sonatype.org/articles/2014/Feb/27/why-the-wait/).

### Decide on a group ID ###
You must pick a group ID that you legitimately control. One of the following is likely to apply.

* **Preferred:** Use your reverse domain name; if you own `example.com`, then your group ID would be `com.example`. This is the **best option** if you own a domain name because you have control over the domain name indefinitely, whereas if you use your code hosting company's reverse domain name (below), you might decide to change to a different code host in the future and your group ID will no longer match.
* *Bitbucket:* If you host your code on [Bitbucket](https://bitbucket.org/), you can use Bitbucket's reverse domain name with your username appended; if your username is `example` on Bitbucket, then your group ID would be `org.bitbucket.example`.
* *GitHub*: If you host your code on [GitHub](https://github.com/), you can use GitHub's reverse domain name with your username appended; if your username is `example` on GitHub, then your group ID would be `com.github.example`.
* *Google Code:* If you host your code on [Google Code](https://code.google.com/), you can use Google Code's reverse domain name with your username appended; if your username is `example` on Google Code, then your group ID would be `com.google.code.example`.

### Request deployment access from Sonatype ###
If you have never done so before, you'll need to [create a Sonatype JIRA account](https://issues.sonatype.org/secure/Signup!default.jspa). Registration is quick and painless.

Once you have a Sonatype JIRA account, [create a New Project ticket](https://issues.sonatype.org/secure/CreateIssue.jspa?issuetype=21&pid=10134) in Sonatype JIRA. This requires a few details.

* **Summary:** Something short and meaningful; try `<group-id> artifacts`, replacing *<group-id>* with your group ID, if you can't think of anything better.
* **Description:** Something like, `Please provide me access to publish artifacts under <group-id> (I own the <domain-name> domain). Thanks!` (Obviously, replace *<group-id>* with your group ID and *<domain-name>* with your domain name; omit the part about owning the domain name if your group ID is based on your code host.)
* **Group ID:** The group ID you've picked for your project. **Note:** If you've picked a more specific group ID for your specific project, e.g., `com.example.utils.formatter` or `org.bitbucket.example.utils.formatter`, you'll want to enter the **most basic** group ID you control here, such as `com.example` or `org.bitbutcket.example`. You'll be granted access to deploy under that group ID and *any* subgroup ID.
* **Project URL:** URL for your project, which could be the overview page for your project on your code host.
* **SCM URL:** The URL for your project's source control (Git, Mercurial, etc.). **Note:** Only `https://` URLs are accepted; don't try to use an `ssh://` URL.
* **Already Synched to Central:** Presumably, this is a new project that you've never published to central, so choose **No**.

After you submit the ticket, watch it to see if the Sonatype folks have any questions, and to see when they mark it as Resolved.

### Create and publish your PGP key ###
Artifacts published to the Central Repository must be signed via PGP.

TODO: Finish this section.

### Develop or copy in your source code ###
Obviously, since this is a stub project, it doesn't contain any source code until you add some.

Note that there is a script called `generate-classes.gradle` that can be handy if you need to trigger some code generation as part of your project.

### Deploy your artifacts to the staging repository ###
**Only attempt this if your Sonatype JIRA ticket has been marked as Resolved!**

TODO: Finish this section.